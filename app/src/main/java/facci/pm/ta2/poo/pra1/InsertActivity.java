package facci.pm.ta2.poo.pra1;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.SaveCallback;
import java.io.IOException;

public class InsertActivity extends AppCompatActivity {

    Button buttonSelectImage, buttonInsert;
    ImageView imageViewFoto;
     private static final int CODIGO = 10;
     EditText editTextPrecio;
    EditText editTextNombre;
    EditText editTextDescripcion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);

        buttonSelectImage = (Button) findViewById(R.id.ButtonSelect);
        buttonInsert = (Button) findViewById(R.id.ButtonInsert);
        imageViewFoto = (ImageView) findViewById(R.id.thumbnail);
        editTextNombre=(EditText) findViewById(R.id.editTextNombre);
        editTextDescripcion= (EditText) findViewById(R.id.editTextDescripcion);
        editTextPrecio= (EditText) findViewById(R.id.editTextPrecio);



       buttonSelectImage.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {


                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setType("image/");
                startActivityForResult(Intent.createChooser(intent,"seleccione la imagen"),CODIGO);

    }
});


        buttonInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final DataObject objeto = new DataObject("item");

                imageViewFoto.buildDrawingCache();
                Bitmap bmap = imageViewFoto.getDrawingCache();
                String string= editTextPrecio.getText().toString();
                String string1= editTextNombre.getText().toString();
                String string2= editTextDescripcion.getText().toString();


                objeto.put("price",string);
                objeto.put("name",string1);
                objeto.put("description",string2);
                objeto.put("image",bmap);

                finish();

                Intent intent = new Intent(getApplicationContext(),ResultsActivity.class);
                startActivity(intent);



          objeto.saveInBackground(new SaveCallback<DataObject>() {

              @Override
              public void done(DataObject object, DataException e) {

                  Toast.makeText(getApplicationContext(),"INGRESO CORRECTO",Toast.LENGTH_SHORT).show();

              }
          });




            }
        });
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CODIGO && resultCode == RESULT_OK && data != null && data.getData() != null){
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),uri);
                imageViewFoto.setImageBitmap(bitmap);
            }
            catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //Esconde el teclado cuando se da click en cualquier parte de la Activity que no sea un EditText
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
        return super.onTouchEvent(event);
    }
}
